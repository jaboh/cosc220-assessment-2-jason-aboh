package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    /*
     * Tests are functions that have an @Test annotation before them.
     * The typical format of a test is that it contains some code that does something, and then one
     * or more assertions to check that a condition holds.
     *
     * This is a dummy test just to show that the test suite itself runs
     */
    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    // FIXME: You need to write tests for the two known bugs in the code.

    // Test to check for box completion , if it has 4 lines drawn then it is complete, else it is incomplete.
    @Test
    public void testForBoxCompletion(){
        logger.info("Testing to check that a box is fully drawn, only when it is complete");

        // Create new grid
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2,2,1);

        // Check for empty box
        assertFalse(grid.boxComplete(0,0), "Empty box is incomplete and should not be claimable");

        // Draw vertical line
        grid.drawVertical(0,0,1);
        // Box is incomplete
        assertFalse(grid.boxComplete(0,0),"Box is incomplete and should not be claimable");

        // Draw a horizontal line
        grid.drawHorizontal(0,0,1);
        // Box is incomplete
        assertFalse(grid.boxComplete(0,0),"Box is incomplete and should not be claimable");

        // Draw another vertical line and another horizontal line to complete the box
        grid.drawVertical(1,0,1);
        grid.drawHorizontal(0,1,1);

        // Check for complete box
        assertTrue(grid.boxComplete(0,0), "Box is complete and should be fully drawn and claimable");
    }
        // Test to check if a line can be drawn, and to throw an illegalStateException if the line has already been
        // drawn at the same coordinates (duplicate).
        @Test
        public void TestForDuplicateLinesDrawn(){
            logger.info("Testing to check that it is disallowed to draw over a line that has already been drawn");
            // Create new grid
            DotsAndBoxesGrid grid = new DotsAndBoxesGrid(2,2,1);

            // Test to check if a vertical and horizontal line can be drawn to the grid
            assertFalse(grid.drawVertical(0,0,1));
            assertFalse(grid.drawHorizontal(0,0,1));

            // Test to catch IllegalStateException, when a duplicate vertical line draw attempt is made
            // at the same coordinates.
            assertThrows(
                    IllegalStateException.class,()-> grid.drawVertical(0,0,1)
                    ,"This line has already been drawn");

            // Test to catch IllegalStateException, when a duplicate horizontal line draw attempt is made
            // at the same coordinates.
            assertThrows(
                    IllegalStateException.class, () -> grid.drawHorizontal(0,0,1),
                    "This line has already been drawn");
        }

}
